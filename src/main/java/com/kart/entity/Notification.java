package com.kart.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "notification")
public class Notification {
	
	@Id
	@Column(name = "notificationId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long notificationId;
	
	@Column(name="notificationCategory")
	private String notificationCategory;
	
	@NotNull(message = "notification  is mandatory")
	@Column(name = "notificationName")
	private String notificationName;
	
	@Column(name = "notificationDescription")
	private String notificationDescription;

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getNotificationCategory() {
		return notificationCategory;
	}

	public void setNotificationCategory(String notificationCategory) {
		this.notificationCategory = notificationCategory;
	}

	public String getNotificationName() {
		return notificationName;
	}

	public void setNotificationName(String notificationName) {
		this.notificationName = notificationName;
	}

	public String getNotificationDescription() {
		return notificationDescription;
	}

	public void setNotificationDescription(String notificationDescription) {
		this.notificationDescription = notificationDescription;
	}
	
}
